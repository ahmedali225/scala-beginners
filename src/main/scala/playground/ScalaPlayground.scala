package playground

object ScalaPlayground extends App {

  println("Hello, Scala!")

  def undefinedReturnType(x: Int) = {
    if (x >= 18)
      "Adult"
    else
      0
  }

  println(undefinedReturnType(6))
}
