package lectures.part3fp

import scala.util.Random

object Options extends App {

  val myFirstOption: Option[Int] = Some(4)
  val noOption: Option[Int] = None

  println(myFirstOption)

  // Unsafe APIs
  def unsafeMethod(): String = null

  //  val result = Some(unsafeMethod()) // Wrong, Some must have a value

  val result = Option(unsafeMethod()) // Some or None

  println(result)

  //chained methods

  def backupMethod(): String = "A valid result"

  val chainedResult = Option(unsafeMethod()).orElse(Option(backupMethod()))

  // Design unsafe APIs
  def betterUnsafeMethod(): Option[String] = None

  def betterBackupMethod(): Option[String] = Some("A valid result")

  val betterChainedResult = betterUnsafeMethod() orElse betterBackupMethod()
  println(betterChainedResult.get)

  // functions on Options
  println(myFirstOption.isEmpty)
  println(myFirstOption.get) // /UNSAFE - DO NOT USE THIS

  // map, flatmap, filter
  println(myFirstOption.map(_ * 2))
  println(myFirstOption.filter(x => x > 10))
  println(myFirstOption.flatMap(x => Option(x * 10)))


  val config: Map[String, String] = Map(
    "host" -> "176.45.36.1",
    "port" -> "80"
  )

  class Connection {
    def connect = "Connected"
  }

  object Connection {

    val random = new Random(System.nanoTime())

    def apply(host: String, port: String): Option[Connection] =
      if (random.nextBoolean()) Some(new Connection)
      else None
  }

  // try to establish a connection

  val host = config.get("host")
  val port = config.get("port")

  /*
  if (h != null)
    if (p != null)
      return connection.apply(h, p)

   return null
   */
  val connection = host.flatMap(h => port.flatMap(p => Connection.apply(h, p)))

  /*
  if (c != null)
    return c.connect
  return null
   */
  val connectionStatus = connection.map(c => c.connect)

  // if (connectionStatus == null) println(None) else print (Some(connectionStatus.get))
  println(connectionStatus)

  /*
  if ( status != null)
    println(status)
   */
  connectionStatus.foreach(println)

  // chained
  config.get("host")
    .flatMap(h => config.get("port")
      .flatMap(p => Connection(h, p))
      .map(c => c.connect))
    .foreach(println)

  // for-comprehensions

  val connectionStatusFor = for {
    host <- config.get("host")
    port <- config.get("port")
    connection <- Connection(host, port)
  } yield connection.connect
  connectionStatusFor.foreach(println)
}


