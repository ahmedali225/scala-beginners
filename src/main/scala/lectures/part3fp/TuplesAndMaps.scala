package lectures.part3fp

import scala.annotation.tailrec

object TuplesAndMaps extends App {

  // tuples = finite ordered Lists

  val aTuple = new Tuple2(2, "Hello Scala") // Tuple2[Int, String] = (Int, String)

  // Can group at most 22 elements of different types

  println(aTuple._1)
  println(aTuple.copy(_2 = "Goodbye Java"))
  println(aTuple.swap)

  // Maps - Keys -> Values or a group of Tuples

  val aMap: Map[String, Int] = Map()

  val phonebook = Map(("Jim", 5555), "Daniel" -> 78998, "JIM" -> 9000).withDefaultValue(-1) // to guard against java.util.NoSuchElementException: key not found

  println(phonebook)

  println(phonebook.contains("Jim"))
  println(phonebook("Jim"))
  println(phonebook("Non-ExistingKey"))

  // add a pairing
  val newPairing = "Mary" -> 67878
  val newPhonebook = phonebook + newPairing

  println(newPhonebook)

  // functionals on map
  println(phonebook.map(pair => pair._1.toLowerCase -> pair._2))

  println(phonebook.filterKeys(x => x.startsWith("J")))

  println(phonebook.mapValues(number => "0245-" + number))

  //conversions to other collections
  println(phonebook.toList)
  println(List(("Daniel", 555)).toMap)

  val names = List("Bob", "Jamie", "Daniel", "Mary", "Angela", "Ali", "Jerry")
  println(names.groupBy(name => name.charAt(0)))


  def add(network: Map[String, Set[String]], person: String): Map[String, Set[String]] = {
    network + (person -> Set())
  }

  def friend(network: Map[String, Set[String]], a: String, b: String): Map[String, Set[String]] = {
    val friendsA = network(a)
    val friendsB = network(b)

    network + (a -> (friendsA + b) ) + (b -> (friendsB + a) )
  }

  def unfriend(network: Map[String, Set[String]], a: String, b: String): Map[String, Set[String]] = {
    val friendsA = network(a)
    val friendsB = network(b)

    network + (a -> (friendsA - b) ) + (b -> (friendsB - a) )
  }

  def remove(network: Map[String, Set[String]], person: String): Map[String, Set[String]] = {
    def removeAux(friends: Set[String], networkAcc: Map[String, Set[String]]): Map[String, Set[String]] =
      if (friends.isEmpty) networkAcc
      else removeAux(friends.tail, unfriend(networkAcc, person, friends.head))

    val unfriended = removeAux(network(person), network)
    unfriended - person
  }

  val empty: Map[String, Set[String]] = Map()
  val network = add(add(empty, "Bob"), "Mary")

  println(network)
  println(friend(network, "Bob", "Mary"))
  println(unfriend(friend(network, "Bob", "Mary"), "Bob", "Mary"))
  println(remove(friend(network, "Bob", "Mary"), "Bob"))

  val people = add(add(add(empty, "Bob"), "Mary"), "Jim")
  val jimBob = friend(people, "Bob", "Jim")
  val testNet = friend(jimBob, "Bob", "Mary")

  println(testNet)

  def nFriends(network: Map[String, Set[String]], person: String): Int =
    if(!network.contains(person)) 0
    else network(person).size

  println(nFriends(testNet, "Bob"))


  def mostFriends(network: Map[String, Set[String]]): String =
    network.maxBy(pair => pair._2.size)._1

  println(mostFriends(network))

  def numberOfPeaopleNoFriends(network: Map[String, Set[String]]): Int =
    network.count(pair => pair._2.isEmpty) // equals to  network.filter(pair => pair._2.isEmpty).size
//    network.filterKeys(k => network(k).isEmpty).size

  println(numberOfPeaopleNoFriends(testNet))

  def socialConnection(network: Map[String, Set[String]], a: String, b: String): Boolean = {
    @tailrec
    def bfs(target: String, consideredPeople: Set[String], discoveredPeople: Set[String]): Boolean = {
      if (discoveredPeople.isEmpty) false
      else {
        val person = discoveredPeople.head
        if (person == target) true
        else if (consideredPeople.contains(person)) bfs(target, consideredPeople, discoveredPeople.tail)
        else bfs(target, consideredPeople + person, discoveredPeople.tail ++ network(person))
      }
    }

    bfs(b, Set(), network(a) + a)
  }

  println(socialConnection(testNet, "Bob", "Jim"))
  println(socialConnection(testNet, "Bob", "Mary"))
}
