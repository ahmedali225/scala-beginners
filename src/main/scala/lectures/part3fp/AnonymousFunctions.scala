package lectures.part3fp

object AnonymousFunctions extends App {

//  val doubler = new Function[Int, Int] {
//    override def apply(x: Int): Int = x * 2
//  }
  // equivlant to
  val doubler = (x: Int) => x * 2 // syntactic sugar (LAMBDA)
  val doubler1: Int => Int = (x: Int) => x * 2 // syntactic sugar (LAMBDA)
  val doubler2: Int => Int = x => x * 2 // syntactic sugar (LAMBDA)

  val adder = (a: Int, b: Int) => a + b

  val noParamLambda: () => Int = () => 3

  println(noParamLambda) // The function itself
  println(noParamLambda()) // call to lambda

  // curly braces with lambda

  val stringToInt = { (str: String) =>
    str.toInt
  }

  val niceIncrementer: Int => Int = (x: Int) => x + 1
  val niceIncrementer1: Int => Int = _ + 1

  val niceAdder: (Int, Int) => Int = (a, b) => a + b
  val niceAdder1: (Int, Int) => Int = _ + _ // the type cannot be removed here and each underscore refers to a different parameter, so you cannot use the same one many times


}
