package lectures.part3fp

object WhatsAFunction extends App {

val doubler = new MyFunction[Int, Int] {
                override def apply(element: Int): Int = element * 2
              }

  println(doubler(2))

  //function types (up to 22 parameters

  val stringToIntConverter = new Function[String, Int] {
    override def apply(string: String): Int = string.toInt
  }

  println(stringToIntConverter("3") + 4)

  val adder: ((Int, Int) => Int) = new Function2[Int, Int, Int] {
    override def apply(a: Int, b: Int): Int = a + b
  }

  println(adder(15, 6))

  // Function types Function2[A, B, R] === (A,B) => R

  // exercises

  val concatenator = new ((String, String) => String) {  // syntactic sugar for Function2[String, String, String] { }
    override def apply(str1: String, str2: String): String = str1 + " " + str2
  }

  println(concatenator("hello", "world"))

  val superAdder = new Function1[Int, Function1[Int, Int]] {
    override def apply(x: Int): Function[Int, Int] = new Function[Int, Int] {
      override def apply(y: Int): Int = x + y
    }
  }

  val adder3 = superAdder(3)
  println(adder3(4))
  println(superAdder(3)(4)) // curried function

  val superAdder1 = (x: Int) => (y: Int) => x + y

  println(superAdder1(3)(4))
}

trait MyFunction[A, B] {
  def apply(element: A): B
}