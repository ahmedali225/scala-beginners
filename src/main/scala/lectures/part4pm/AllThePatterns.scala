package lectures.part4pm

import exercises.{Cons, Empty, MyList}

object AllThePatterns extends App {

//  // 1. constants
//  val x: Any = "Scala"
//  val constants = x match {
//    case 1 => "a number"
//    case "Scala" => "The Scala"
//    case true => "The Truth"
//    case AllThePatterns => "A Singlton object"
//  }
//
//  // 2. match anything
//  val matchAnything = x match {
//    case _ =>
//  }
//
//  val matchVariable = x match {
//    case something => s"I've found $something"
//  }
//
//  // 3. tuples
//  val atuple = (1,2)
//
//  val matchTuple = atuple match {
//    case (1, 1) => "hi"
//    case(something, 2) => s"I've found $something"
//  }
//
//  val nestedTuple = (1, (2, 3))
//  val matchNestedTuple = nestedTuple match {
//    case (_, (2, v)) =>  // PMs can be nested!
//  }
//
//  // 4. case classes - constructor pattern
//  // PMs can be nested with case classes as well
//  val aList: MyList[Int] = Cons(1, Cons(2, Empty))
//  val matchAList = aList match {
//    case Empty =>
//    case Cons(head, Cons(subHead, subTail)) =>
//  }
//
//  // 5. list patterns
//  val aStandardList = List(1,2,3,42)
//  val standardListMatching = aStandardList match {
//    case List(1, _, _, _) => // extractor - advanced
//    case List(1, _*) => // a list of arbitrary length (var args) - advanced
//    case 1 :: List(_) => //infix pattern
//    case List(1,2,3) :+ 42 => //infix pattern
//  }
//
//  // 6. type specifiers
//  val unknown: Any = 2
//  val unkownMatch = unknown match {
//    case list: List[Int] => // explicit type specifier
//    case _ =>
//  }
//
//  // 7. name binding
//  val nameBindingMatch = aList match {
//    case nonEmptyList @ Cons(_, _) => // name binding => use the name later/here
//    case Cons(1, rest @ Cons(2, _)) => // name binding inside nested patterns
//  }
//
//  // 8. multi-patterns
//  val multiPattern = aList match {
//    case Empty | Cons(0, _) => // Compound pattern ( multi-pattern)
//  }
//
//  // 9. if guards
//  val secondSpecialElement = aList match {
//    case Cons(_, Cons(specialElement, _)) if specialElement % 2 == 0 =>
//  }

  /////
  val numbers = List(1,2,3)
  val numbersMatch = numbers match {
    case listOfStrings: List[String] => "a list of Strings"
    case listOfInts: List[Int] => "a list of Ints"
  }

  println(numbersMatch) // Will return a list of strings as the JVM is not handling matching generics
  // because of backward compatibility, the compiler will erase all generic types which means it matches a List (Type erasure problem)
}
