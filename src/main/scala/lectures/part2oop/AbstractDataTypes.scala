package lectures.part2oop

object AbstractDataTypes extends App {

  // Abstract Classes cannot be instantiated
  abstract class Animal {
    val creatureType: String
    def eat: Unit
  }

  class Dog extends Animal {
    override val creatureType: String = "Canine"

    /*override*/ def eat: Unit = println("Crunch crunch")  // override keyword can be removed
  }

  //traits
  trait Carnivore {
    def eat(animal: Animal): Unit
    val preferredMeal = "Meat"   // Non Abstract
  }

  trait ColdBlooded

  class Crocodile extends Animal with Carnivore with ColdBlooded {
    override val creatureType: String = "croc"

    override def eat: Unit = "nomnomnom"

    override def eat(animal: Animal): Unit = println(s"I'm a croc and i'm eating ${animal.creatureType}")
  }

  val dog = new Dog

  val croc = new Crocodile

  croc.eat(dog)

  //Traits vs Abstract Classes
  //1. traits don't have constructor parameters
  //2. multiple traits may be inherited by the same class, unlike abstract classes
  //3. traits = behavior, abstract class = "thing"

}
