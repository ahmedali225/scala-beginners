package lectures.part2oop

import playground.{Cinderlla, PrinceCharming => Prince}

import java.sql.{Date => SqlDate}
import java.util.Date

object PackagingAndImports extends App {

  val writer = new Writer("Daniel", "Rock the JVM", 2018)

  val princess = new Cinderlla //playground.Cinderlla // Fully qualified name

  // package object
  // is to have universal constants or methods
  // Can only be one per package
  // its name is the same as the package it reside in
  // File name is package.scala

  sayHello
  println(SPEED_OF_LIGHT)

  // imports

  val prince = new Prince

  val d = new Date // The compiler will assume by default that it's the first one
//  val sqlDate = new java.sql.Date(2018, 8, 5) // The compiler will assume by default that it's the first one
  val sqlDate = new SqlDate(2018, 8, 5) // The compiler will assume by default that it's the first one

  // Default Imports
  // java.lang - String, Object, Exception
  // top-level scala package - Int, Nothing, Function
  // scala.predef - println, ???
}
