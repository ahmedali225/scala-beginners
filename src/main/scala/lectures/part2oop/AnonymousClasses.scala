package lectures.part2oop

import lectures.part2oop.AnonymousClasses.Animal

object AnonymousClasses extends App {

  abstract class Animal {
    def eat: Unit
  }

  val funnyAnimal: Animal = new Animal {
    override def eat: Unit = println("Anonymous class implementation")
  }

  println(funnyAnimal.getClass)

  //what the compiler actually does
  /*class AnonymousClasses$$anon$1 extends Animal {
    override def eat: Unit = println("Anonymous class implementation")
  }*/

  class Person (name: String) {
    def sayHi: Unit = println(s"Hi, my name is $name, how can i help?")
  }

  val jim = new Person("Jim") {
    override def sayHi: Unit = super.sayHi //println(s"Hi, my name is $name, how can i help?")
  }
}
