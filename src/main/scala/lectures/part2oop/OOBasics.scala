package lectures.part2oop

object OOBasics extends App {

  val person = new Person("Mega", 31)
  val person1 = new Person("Mega")  // Won't work without the auxiliary constructor defined or default value in the main constructor
  val person2 = new Person  // Won't work without the default constructor being defined

  println(person.age)
  println(person.x)
  person.greet("Daniel")
  person.greet()

  /////// Exercises

  val author = new Writer("Ahmed", "Ali", 1987)
  println(author.fullName)

  val anotherAuthor = new Writer("Khalid", "Tawfik", 1962)

  val novel = new Novel("Hunger Games", 2005, author)
  println(novel.authorAge)
  println(novel.isWrittenByAuthor(author))
  println(novel.isWrittenByAuthor(anotherAuthor))

  val anotherNovel = novel.copy(2019)
  println(novel.authorAge)
  println(anotherNovel.authorAge)

  val count = new Counter(4)
  count.increment
  println(count.currentCount)
  count.increment(16)
  println(count.currentCount)
  println(count.increment)
  println(count.currentCount)

  val immutableCounter = new ImmutableCounter(1200)
  println(immutableCounter.increment.currentCount)
  println(immutableCounter.increment(1200).currentCount)

  immutableCounter.increment.print
  immutableCounter.increment.increment.increment.print
}

// Constructor
// class parameters are not fields
// Add val or var to make them fields
class Person(name: String, val age: Int) {

  val x = 2

  println(1 + 3) // the whole block gets evaluated first

  def greet(name: String): Unit = println(s"${this.name} says: Hi, $name")

  // Overloading
  def greet(): Unit = println(s"Hi, I am $name") // This will be implied to refer the parameter

//  def greet(): Int = 42 // Different return type will not work, it has to be a different signature (number of parameters or their types)

  // Multiple constructors
  def this(name: String) = this(name, 0) // (Auxiliary) These are not practical, use default values in main constructor instead
  def this() = this("John Doe")  // Default constructor
}


/*
Novel and a writer class
 */

class Writer(firstName: String, surname: String, val year: Int) {

  def fullName: String = s"$surname, $firstName"
}

class Novel(name: String, yearOfRelease: Int, author: Writer) {

  def authorAge = yearOfRelease - author.year

  def isWrittenByAuthor(author: Writer) = this.author.eq(author)

  def copy(newReleaseYear: Int) = new Novel(name, newReleaseYear, author)
}


/*
Counter class
 */

class Counter(var value: Int) {

  def currentCount: Int = value

  def increment: Unit = value += 1
  def increment(amount: Int): Unit = value += amount

  def decrement: Unit = value -= 1
  def decrement(amount: Int): Unit = value -= amount
}

class ImmutableCounter(val value: Int = 0) {

  def currentCount= value

  def increment= new ImmutableCounter(value + 1)

  def increment(amount: Int): ImmutableCounter = { // If needed a for loop
    if (amount <= 0) this
    else increment.increment(amount - 1)  // increment will return an instance of the class, so we pass (n - 1)
  }

  def decrement = new ImmutableCounter(value - 1)

  def decrement(amount: Int): ImmutableCounter = {
    if (amount <= 0) this
    else decrement.decrement(amount - 1)
  }

  def print = println(value)
}