package lectures.part2oop

object Inheritance extends App {

  //Single Class Inheritance
  sealed class Animal {
    val creatureType = "wild"
    def eat = println("nomnom")
  }

  class Cat extends Animal {

    def crunch = {
      eat
      println("crunch crunch")
    }
  }

  val cat = new Cat
//  cat.eat
  cat.crunch

  //default modifier is public

  //Constructors

  /*final */class Person(name: String, age: Int) {
    def this(name: String) = this(name, 0)
  }
  class Adult(name: String, age: Int, idCard: String) extends Person(name/*, age*/)

  //Overriding
  class Dog(override val creatureType: String) extends Animal {  // You can override properties in the constructor
//    override val creatureType: String = "domestic"
    override def eat: Unit = {
      super.eat
      println("dog crunch")
    }
  }

  val dog = new Dog("K9")
  dog.eat
  println(dog.creatureType)

  // Type substitution (Polymorphism)
  val unknownAnimal: Animal = new Dog("newDog")
  unknownAnimal.eat

  // Super

  //Preventing override
  // 1. final with members
  // 2. final with classes (Prevent the entire class from being extended)
  // 3. seal the class = extend classes in this file and prevent in any other file
}
