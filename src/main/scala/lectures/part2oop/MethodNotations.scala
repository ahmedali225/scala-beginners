package lectures.part2oop

object MethodNotations extends App {

  class Person(val name: String, favouriteMovie: String, val age: Int = 0 /*Exercises*/) {

    def likes(movie: String): Boolean = movie == favouriteMovie
//    def hangoutWith(person: Person): String = s"${this.name} hanging out with ${person.name}"  // can be replaced with the below
    def +(person: Person): String = s"${this.name} hanging out with ${person.name}" // valid method
    def +(string: String): Person = new Person(s"${this.name} ($string)", favouriteMovie) // exercises

    def unary_! : String  = s"$name returned!"// add space before colon
    def unary_+ : Person  = new Person(this.name, this.favouriteMovie, this.age + 1)// exercises

    def apply(): String = s"Hi, my name is $name and i like $favouriteMovie"
    def apply(numberOfTimes: Int): String = s"$name watched $favouriteMovie $numberOfTimes times" //exercises

    def isAlive: Boolean = true  // doesn't receive any parameter, hence can be used in postfix notation
    def learns(thing: String) : String = s"${this.name} learns ${thing}"  // exercises
    def learnsScala = this learns s"Scala"
  }

  val mary = new Person("Mary", "Inception")
  println(mary.likes("Inception"))
  println(mary likes "Inception") //equivalent
  //Infix notation = operation notation (syntactic sugar) -> only works with methods that takes only one parameter

  // "operators" in Scala
 val tom = new Person("Tom", "Fight Club")
  println(tom + mary)
  println(tom.+(mary))

  println(1+2)
  println(1.+(2))  //all operators are methods

  // AKKA actors have ! ?

  // prefix notation (All about unary operators)
  val x = -1
  val y = 1.unary_- // Only works with a few operators - + ~ !

  println(!mary)
  println(mary.unary_!)

  // postfix notation

  println(mary.isAlive)
  println(mary isAlive)

  // apply

  println(mary.apply())
  println(mary())

  ////Exercises

  // overload +
  println((mary + "the rock start")()) // calling the apply method

  println((+mary).age) // unary operator

  println(mary learnsScala) // Postfix notation

  println(mary(10)) // uses apply method with the parameter
}
