package lectures.part2oop

object Generics extends App {

  class MyList[+A] { // can be also used with traits
//    def add(element: A): MyList[A] = ???   // Covariant issue
    def add[B >: A](element: B): MyList[B] = ???
    /*
    A = Cat
    B = Animal
     */
  }

  class MyMap[key, value]

  val listOfInts = new MyList[Int]
  val listOfStrings = new MyList[String]

  //generic methods
  object MyList { // cannot be parametrized

    def empty[A]: MyList[A] = ???
  }

  val myEmptyListOfInts = MyList.empty[Int]

  // variance problem

  class Animal
  class  Cat extends Animal
  class Dog extends Animal

  //1. Yes, List[Cat] extends List[Animal] = Covariance
  class Covariant[+A]
  val animal: Animal = new Cat
  val animalList: Covariant[Animal] = new Covariant[Cat]
//  animalList.add(new Dog) ??? Hard Question  => We return a list of animals

  //2. No = Invariance
  class InvariantList[A]
  val invariantAnimalList: InvariantList[Animal] = new InvariantList[Animal] // new InvariantList[Cat] // This will give a compilation error

  //3. Hell, No! Contravariant
  class Trainer[-A]
  val trainer: Trainer[Cat] = new Trainer[Animal]

  // bounded Types
  // (A <: subtype/extends of ...) upper bounded
  // (A >: super of ...) lower bounded
  class Cage[A <: Animal](animal: A)
  val cage = new Cage(new Dog)

  class Car
//  val newCage = new Cage(new Car) // compilation error


  // expand MyList to be Generic
}
