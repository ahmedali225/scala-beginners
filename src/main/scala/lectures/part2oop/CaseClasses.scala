package lectures.part2oop

object CaseClasses extends App {

  // Case classes is a shorthand for defining class and the companion object with the minimum hustle

  // equals, hashCode, toString
  case class Person(name: String, age: Int)

  // 1. class parameters are auto promoted to fields
  val jim = new Person("Jim", 34)
  println(jim.name)

  // 2. sensible toString
  // println(instance) = println(instance.toString) (syntactic sugar)
  println(jim.toString) // = println(jim)

  //3. equals and hashCode implemented OOTB
  // Will not compare references equality in this case
  val jim2 = new Person("Jim", 34)
  println(jim == jim2)

  // 4. CCs have handy copy method
  val jim3 = jim.copy(age = 45)
  println(jim3)

  // 5. CCs have companion objects
  val thePerson = Person
  val mary = Person("Mary", 23)  // handy Apply/Factory method

  // 6. CCs are serializable
  // Akka

  // 7. CCs have extractor patterns = CCs can be used in PATTERN MATCHING


  // This acts like a case class and a case object
  case object UnitedKingdom {
    def name: String = "The UK of GB and NI"
  }


}
