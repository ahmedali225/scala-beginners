package lectures.part2oop

object Objects extends App {

  // SCALA DOESN'T HAVE CLASS-LEVEL FUNCTIONALITY ("static")

  // Objects can be treated the same way a class is except for having parameters
  object Person {  // type + its only instance
    // Static/Class-level functionality
    val N_EYES = 2
    def canFly: Boolean = false

    // a factory methods
    def from(mother: Person, father: Person): Person = new Person("Bobbie")
    def apply(mother: Person, father: Person): Person = new Person("Bobbie")  // Mostly used pattern
  }

  class Person(val name: String) {
    // Instance-level functionality
  }
  // Both are COMPANIONS == same scope and the same name

  println(Person.N_EYES)
  println(Person.canFly)

  // Scala Object = SINGLETON INSTANCE

  val mary = Person
  val john = Person

  println(mary == john) // true as both refer to the same instance

  val mary1 = new Person("Mary")
  val john1 = new Person("John")
  println(mary1 == john1) // false as both refer to different instances after instantiation

  val bobbie = Person.from(mary1, john1) // Or use the apply method way as factory
  val bobbie1 = Person(mary1, john1)

  // Scala Applications = Scala Object with
  // def main(args: Array[String]): Unit

}
