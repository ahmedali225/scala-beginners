package lectures.part2oop

object Exceptions extends App {

  val s: String = null
//  println(s.length) // This will throw NullPointerException

//  val aWeirdVal: String = throw new NullPointerException

  // Throwable
  // Exception and Error are major throwable subtypes

  // catching exceptions
  def getInt(withExceptions: Boolean): Int =
    if (withExceptions) throw new RuntimeException("No Int for you!")
    else 42

  val potentialFail = try {  // The value of potentialFail is AnyVal (Int and Unit)
    getInt(false)
  } catch {
    case e: RuntimeException => { println("caught a runtime exception: " + e.getMessage) }
//    case e: NullPointerException => { println("caught a Null pointer exception: " + e.getMessage) }
  } finally { // finally block is optional and doesn't affect the return type of this block
    // Code will be executed no matter what!
    println("finally")
  }

  println(potentialFail)

  // How to define your own Exceptions
  class MyException extends Exception
  val exception = new MyException

//  throw exception


  // Exercises

  // OOM
//  val array = Array.ofDim(Int.MaxValue)

  // SO
//  def infinite: Int = 1 + infinite
//  val noLimit = infinite


  // Int.MaxValue can't be compared in an if statement
  // if(x + y  > Int.MaxValue)
  object PocketCalculator {

    def add(x: Int, y: Int): Int = {
      val result = x + y
      if (x > 0  && y > 0 && result < 0) throw new OverflowException("Result exceeds Max Val")
      else if (x < 0  && y < 0 && result > 0) throw new UnderflowException("Result exceeds Max Val")

      result
    }

    def subtract(x: Int, y: Int): Int = {
      val result = x - y
      if (x > 0  && y < 0 && result < 0) throw new OverflowException("Result exceeds Max Val")
      else if (x < 0  && y > 0 && result > 0) throw new UnderflowException("Result exceeds Max Val")

      result
    }

    def multiply(x: Int, y: Int): Int = {
      val result = x + y
      if (x > 0  && y > 0 && result < 0) throw new OverflowException("Result exceeds Max Val")
      else if (x < 0  && y < 0 && result < 0) throw new OverflowException("Result exceeds Max Val")
      else if (x > 0  && y < 0 && result > 0) throw new UnderflowException("Result exceeds Max Val")
      else if (x < 0  && y > 0 && result > 0) throw new UnderflowException("Result exceeds Max Val")

      result
    }

    def divide(x: Int, y: Int): Int =
      if(y == 0) throw new MathCalculationException("CAN'T DIVIDE BY ZERO")
      else x / y

  }

  case class OverflowException(msg: String) extends RuntimeException(msg)
  case class UnderflowException(msg: String) extends RuntimeException(msg)
  case class MathCalculationException(msg: String) extends RuntimeException(msg)

//  println(PocketCalculator.add(Int.MaxValue, 10))
  println(PocketCalculator.divide(2, 0))
}
