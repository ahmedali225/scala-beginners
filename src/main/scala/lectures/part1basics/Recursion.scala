package lectures.part1basics

import scala.annotation.tailrec

object Recursion extends App {

  // The JVM keep all the calls needed of the recursive function on it's Stack until it finishes and releases all of them at once
  def factorial(n: Int): Int =
    if (n <= 1) 1
    else {
      println("Computing factorial of " + n + " - I first need a factorial of " + (n-1))
      val result = n * factorial(n-1)
      println("Computed factorial of " + n)

      result
    }

  println(factorial(10))
//  println(factorial(5000)) //StackOverFlow error

  def anotherFactorial(n: Int): BigInt = {
    @tailrec // That means that the recursion is the last block of its code path
    def factorialHelper(x: Int, accumulator: BigInt): BigInt =  // The trick here is to use the same Stack Frame and replace the same instead of creating a new one in memory (TAIL RECURSION)
      if (x <= 1) accumulator
      else factorialHelper(x - 1, x * accumulator)

    factorialHelper(n, 1)
  }

  println(anotherFactorial(5000))

  @tailrec
  def concatString(aString: String, x: Int, accumulator: String): String = {
    if (x <= 1) accumulator
    else concatString(aString, x - 1, aString + accumulator)
  }

  println(concatString("Mega ", 10, ""))

  def isPrime(n: Int): Boolean = {
    @tailrec
    def isPrimeTailrec(t: Int, isStillPrime: Boolean): Boolean = // Auxiliary function
      if (!isStillPrime) false
      else if (t <= 1) true
      else isPrimeTailrec(t - 1, n % t != 0 && isStillPrime)

    isPrimeTailrec(n / 2, true)
  }

  println(isPrime(2003))
  println(isPrime(629))

  def fibonacciFunction(n: Int): Int = { // Number of accumulators would be based upon the number of method calls needed recursivly
    @tailrec
    def fiboTailrec(i: Int, last: Int, nextToLast: Int): Int =
      if (i >= n) last
      else fiboTailrec(i + 1, last + nextToLast, last)

    if (n <= 2) 1
    else fiboTailrec(2, 1, 1)
  }
  // 1 1 2 3 5 8 13 21
  println(fibonacciFunction(8))

}
