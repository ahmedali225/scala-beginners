package lectures.part1basics

object Functions extends App {

  def aFunction(a: String, b: Int): String = {  // The compiler can figure out the return type of a function if not specified
    a + " " + b
  }

  println(aFunction("Hello", 3))

  def aParameterlessFunction(): Int = 42

  println(aParameterlessFunction())
  println(aParameterlessFunction)

  def aRepeatedFunction(aString: String, n: Int): String = {  //recursion

    if (n == 1) aString
    else aString + aRepeatedFunction(aString, n -1)
  }
  // The compiler cannot figure out the return type of a recursive function if not specified

  println(aRepeatedFunction("Hello ", 3))

  //WHEN YOU NEED LOOPS, USE RECURSION


  def aFunctionWithSideEffects(aString: String): Unit = println(aString)

  // Defining and using functions inside a code block
  def aBigFunction(n: Int): Int = {
    def aSmallerFunction(a: Int, b: Int): Int = a + b

    aSmallerFunction(n, n - 1)
  }

  def greetingsFunction(name: String, age: Int) = {
    "Hi, my name is " + name + " and I am " + age + " years old."
  }

  println(greetingsFunction("David", 12))

  def getFactorial(n: Int): Int = {

    if ( n <= 0) 1
    else n * getFactorial(n - 1)
  }

  println(getFactorial(5))

  def fibonacciFunction(n: Int): Int = {
    if (n <= 2) 1
    else fibonacciFunction(n-1) + fibonacciFunction(n-2)
  }
  // 1 1 2 3 5 8 13 21
  println(fibonacciFunction(8))

  def isPrime(n: Int): Boolean = {
    def isPrimeUntil(t: Int): Boolean = // Auxiliary function
      if (t <= 1) true
      else n % t != 0 && isPrimeUntil(t - 1)

    isPrimeUntil(n / 2)
  }

  println(isPrime(37))
  println(isPrime(2003))
  println(isPrime(37 * 17))
}
