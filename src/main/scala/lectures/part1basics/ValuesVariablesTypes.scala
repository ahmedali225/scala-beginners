package lectures.part1basics

object ValuesVariablesTypes extends App {

  // vals are immutable like constants and finals in Java
  // The types of vals are optional and the compiler can infer it
  val x = 42
  println(x)

  val aString: String = "hello"; //semicolon are allowed in Scala and it would be mandatory if two expressions are in the same line
  val anotherString = "goodbye"

  val aBoolean: Boolean = false
  val aChar: Char = 'a'
  val anInt: Int = x
  val aShort: Short = 4613 // 2 bytes , half the size of Int
  val aLong: Long = 7358785478487458548L // 8 bytes
  val aFloat: Float = 2.0f //Without an f the compiler will create a Double
  val aDouble: Double = 3.14

  // Variables
  //Can be reassigned
  var aVariable: Int = 4

  aVariable = 5 // side effects

}
