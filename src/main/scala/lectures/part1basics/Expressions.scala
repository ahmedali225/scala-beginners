package lectures.part1basics

object Expressions extends App {

  val x = 1 + 2 //Expression
  println(x)

  println(2 + 3 * 4)
  // + - * / & | ^ << >> >>> (right shift with zero extension)

  println(1 == x)
  // == != > >= < <=

  println(!(1 == x))
  // ! && ||

  var aVariable = 2
  aVariable += 3 //also works -= *= /=  ..... side effects (only works with variables)

  println(aVariable)

  //Instructions (Do) vs Expressions (VALUE)

  //IF Expression
  val aCondition = true
  val aConditionedValue = if (aCondition) 5 else 3 // IF Condition
  println(aConditionedValue)
  println(if (aCondition) 5 else 3 )
  println(1 + 3)

  var i = 0
  while (i < 10) { //avoid imparitive programming

    println(i)
    i += 1
  }

  //Everything in Scala is an Expression

  val aWeirdValue = (aVariable = 3) // Unit === Void
  println(aWeirdValue)

  // side effects: println(), whiles, reassigning

  //Code block

  val aCodeblock = {  // It's value and type equals the last expression in the block
    val y = 2  //inner scope
    val z = y + 1 // inner scope

    if (z > 2) "hello" else "goodbye"
  }

  println(aCodeblock)

  val someValue = {
    2 < 3
  }

  val someOtherValue = {
    if (someValue) 239 else 986
    42
  }

  println(someValue)
  println(someOtherValue)
}
