package lectures.part1basics

object StringOps extends App {

  val str: String = "Hello, I am learning Scala!"

  println(str.charAt(2)) // Zero indexed
  println(str.substring(7, 11))
  println(str.split(" ").toList)
  println(str.startsWith("Hello"))
  println(str.replace(" ", "-"))
  println(str.toLowerCase())
  println(str.length)

  val aNumberString = "45"
  val aNumber = aNumberString.toInt

  println('a' +: aNumberString :+ 'z')
  println(str.reverse)
  println(str.take(2))

  // Scala specific

  // S-interpolated
  val name = "David"
  val greeting = s"Hello, my name is $name and age is $age"
  val anotherGreeting = s"Hello, my name is $name and age is ${age + 1}"
  println(anotherGreeting)
  val speedPrecision = 1.2f

  // F-interpolated
  val age = 12
  val myth = f"$name can eat $speedPrecision%2.4f burgers per minute"
  val myth2 = f"$name can eat $speedInt%3d burgers per minute"      // Using val of type float will cause compilation error
  val speedInt = 4
  println(myth)
  println(myth2)

  // raw-interpolated
  println(raw"This is a \n newline") // ignores escaped characters

  val escaped = "This is a \n newline"
  println(raw"$escaped")    // escape characters will not be ignored this way
}
