package exercises

abstract class MyList[+A] {

//  var list = new Array[Int](1)

  def head: A

  def tail: MyList[A]

  def isEmpty: Boolean

  def add[B >: A](item: B) : MyList[B]

  // Polymorphic call
  override def toString: String = "[" + printElements + "]"//super.toString

  def printElements: String

  // Higher order functions - must have the below signature in order to use with for-comprehensions
  def map[B](transformer: A => B): MyList[B]
  def flatMap[B](transformer: A => MyList[B]): MyList[B]
  def filter(predicate: A => Boolean): MyList[A]
  def ++[B >: A](list: MyList[B]): MyList[B]  // Concatenation function for flatmap

  //HOFs exercises
  def foreach(f: A => Unit): Unit
  def sort(compare: (A, A) => Int): MyList[A]
  def zipWith[B, C](list: MyList[B], zip: (A, B) =>  C): MyList[C]
  def fold[B](start: B)(operator: (B, A) => B): B
}

case object Empty extends MyList [Nothing] {

  override def head: Nothing = throw new NoSuchElementException

  override def tail: MyList[Nothing] = throw new NoSuchElementException

  override def isEmpty: Boolean = true

  override def add[B >: Nothing](item: B): MyList[B] = new Cons(item, Empty)

  override def printElements: String = ""

  override def map[B](transformer: Nothing => B): MyList[B] = Empty

  override def flatMap[B](transformer: Nothing => MyList[B]): MyList[B] = Empty

  override def filter(predicate: Nothing => Boolean): MyList[Nothing] = Empty

  override def ++[B >: Nothing](list: MyList[B]): MyList[B] = list

  override def foreach(f: Nothing => Unit): Unit = () // the Unit Value

  override def sort(compare: (Nothing, Nothing) => Int): MyList[Nothing] = Empty

  override def zipWith[B, C](list: MyList[B], zip: (Nothing, B) => C): MyList[C] =
    if (!list.isEmpty) throw new RuntimeException("Lists don't have the same length")
    else Empty

  override def fold[B](start: B)(operator: (B, Nothing) => B): B = start
}

case class Cons[+A](h: A, t: MyList[A]) extends MyList[A] {

  override def head: A = h

  override def tail: MyList[A] = t

  override def isEmpty: Boolean = false

  override def add[B >: A](item: B): MyList[B] = new Cons(item, this)

  override def printElements: String =  // recursive
    if (t.isEmpty) "" + h
    else h + " " + t.printElements

  /*
  [1,2,3].map(n * 2)
  = new Cons(2, [2,3].map(n * 2))
  = new Cons(2, new Cons(4, [3].map(n * 2))
  = new Cons(2, new Cons(4, new Cons(6, Empty.map(n * 2))))
  = new Cons(2, new Cons(4, new Cons(6, Empty))
   */
  override def map[B](transformer: A => B): MyList[B] =
    new Cons(transformer.apply(h), t.map(transformer))

  /*
  [1,2,3].filter( n % 2 == 0) =
  [2,3]..filter( n % 2 == 0) =
  = new Cons(2, [3].filter( n % 2 == 0))
  = new Cons(2, Empty.filter( n % 2 == 0))
  = new Cons(2, Empty)
   */
  override def filter(predicate: A => Boolean): MyList[A] =
    if (predicate.apply(h)) new Cons(h, t.filter(predicate))
    else t.filter(predicate)

  /*
  [1,2] ++ [3,4,5]
  = new Cons(1, [2] ++ [3,4,5])
  = new Cons(1, new Cons(2, Empty ++ [3,4,5]))
  = new Cons(1, new Cons(2, new Cons(3, new Cons([4] ++ [5]))))
  */
  override def ++[B >: A](list: MyList[B]): MyList[B] = new Cons(h, t ++ list)

  /*
    [1,2].flatMap(n => [n,n+1])
    = [1,2] ++ [2].flatMap(n => [n,n+1])
    = [1,2] ++ [2,3] ++ Empty.flatMap(n => [n,n+1])
    = [1,2] ++ [2,3] ++ Empty
   */
  override def flatMap[B](transformer: A => MyList[B]): MyList[B] =
    transformer.apply(h) ++ t.flatMap(transformer)

  override def foreach(f: A => Unit): Unit = {
    f(h)
    t.foreach(f)
  }

  override def sort(compare: (A, A) => Int): MyList[A] = {
    def insert(x: A, sortedList: MyList[A]): MyList[A] =
      if (sortedList.isEmpty) new Cons(x, Empty)
      else if (compare(x, sortedList.head) <= 0 ) new Cons(x, sortedList)
      else new Cons(sortedList.head, insert(x, sortedList.tail))

    val sortedTail = t.sort(compare)
    insert(h, sortedTail)
  }

  override def zipWith[B, C](list: MyList[B], zip: (A, B) => C): MyList[C] = {
    if (list.isEmpty) throw new RuntimeException("Lists don't have the same length")
    else new Cons(zip(h, list.head), t.zipWith(list.tail, zip))
  }

  override def fold[B](start: B)(operator: (B, A) => B): B = {
//    val newStart = operator(start, h)
//    t.fold(newStart, operator)
    t.fold(operator(start, h))(operator) // equivalent to the above two lines
  }
}

// Exercises

//trait MyPredicate[-T] { // T => Boolean
//  def test(t: T): Boolean
//}

//trait MyTransformer[-A, B] { //  A => B
//  def transform(a: A): B
//}

object ListTest extends App {

  val listOfInts: MyList[Int] = Empty
  val listOfInts1: MyList[Int] = new Cons(1, new Cons(2, new Cons(3, Empty)))
  val listOfInts2: MyList[Int] = new Cons(4, new Cons(5, Empty))
  val listOfString: MyList[String] = new Cons("Hello", new Cons("Scala", Empty))
  val cloneListOfString: MyList[String] = new Cons("Hello", new Cons("Scala", Empty))

  println(listOfInts1.toString)
  println(listOfString.toString)

//  println(listOfInts1.map(new Function1[Int, Int] {
//    override def apply(elem: Int): Int = elem * 2
//  }))
  println(listOfInts1.map(elem => elem * 2)) // equivalent lambda expression of the above
  println(listOfInts1.map(_ * 2)) // equivalent lambda expression of the above

  println(listOfInts1.filter(elem => elem % 2 == 0))
  println(listOfInts1.filter(_ % 2 == 0))

  println(listOfInts1 ++ listOfInts2)  // Concatenation

  println(listOfInts1.flatMap(elem => new Cons(elem, new Cons(elem + 1, Empty))).toString)
//  println(listOfInts1.flatMap(elem => new Cons(elem, new Cons(elem + 1, Empty)))) // Compilation error that i don't understand

  println(cloneListOfString == listOfString) // after adding Case keyword to Empty and Cons

  listOfInts1.foreach(println)
  println(listOfInts1.sort((x, y) => y - x))

  println(listOfInts2.zipWith[String, String](listOfString, _ + " - " + _))
  println(listOfInts1.fold(0)(_ + _))
  /////////////

  val cominations = for {
    n <- listOfInts1
    string <- listOfString
  } yield n + "-" + string

  println(cominations)
}
